<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Column;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\MaxDepth;

/**
 * Device
 */
class Device
{

    use Column\Id;
    use Column\Code;
    use Column\Password;

    /**
     * @var
     */
    private $type;

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param mixed $model
     */
    public function setModel($model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function getInventoryNumber()
    {
        return $this->inventoryNumber;
    }

    /**
     * @param mixed $inventoryNumber
     */
    public function setInventoryNumber($inventoryNumber)
    {
        $this->inventoryNumber = $inventoryNumber;
    }

    /**
     * @return mixed
     */
    public function getWorkshop()
    {
        return $this->workshop;
    }

    /**
     * @param mixed $workshop
     */
    public function setWorkshop($workshop)
    {
        $this->workshop = $workshop;
    }

    /**
     * Constructor
     */
    public function __construct()
    {

    }

    /**
     * @var \DateTime
     */
    private $manufacturingDate;


    private $model;

    private $inventoryNumber;

    /**
     * @MaxDepth(0)
     * @var
     */
    private $workshop;

    /**
     * @Exclude()
     * @var \Doctrine\Common\Collections\Collection
     */
    private $commands;

    /**
     * @param \DateTime $date
     *
     * @return static
     */

    public function setManufacturingDate($date)
    {
        $this->manufacturingDate = $date;

        return $this;
    }

    /**
     * @return \DateTime
     */

    public function getManufacturingDate()
    {
        return $this->manufacturingDate;
    }

    /**
     *
     * @Exclude()
     * @var \Doctrine\Common\Collections\Collection
     */
    private $sensors;


    /**
     * Add sensor
     *
     * @param Sensor $sensor
     *
     * @return Device
     */
    public function addSensor(Sensor $sensor)
    {
        $this->sensors[] = $sensor;

        return $this;
    }

    /**
     * Remove sensor
     *
     * @param Sensor $sensor
     */
    public function removeSensor(Sensor $sensor)
    {
        $this->sensors->removeElement($sensor);
    }

    /**
     * Get sensors
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSensors()
    {
        return $this->sensors;
    }


    /**
     * Add command
     *
     * @param Command $command
     *
     * @return Device
     */
    public function addCommand(Command $command)
    {
        $this->commands[] = $command;

        return $this;
    }

    /**
     * Remove command
     *
     * @param Command $command
     */
    public function removeCommand(Command $command)
    {
        $this->commands->removeElement($command);
    }

    /**
     * Get commands
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCommands()
    {
        return $this->commands;
    }

    /**
     * @var string
     */
    private $enabledSensors;


    /**
     * Set enabledSensors
     *
     * @param string $enabledSensors
     *
     * @return Device
     */
    public function setEnabledSensors($enabledSensors)
    {
        $this->enabledSensors = json_encode($enabledSensors);

        return $this;
    }

    /**
     * Get enabledSensors
     *
     * @return string
     */
    public function getEnabledSensors()
    {
        return json_decode($this->enabledSensors);
    }

    /**
     * Implement toString magic method
     *
     * @return mixed
     */
    public function __toString()
    {
        return $this->getCode();
    }

    /**
     * @var \AppBundle\Entity\WorkCenter
     */
    private $workcenter;


    /**
     * Set workcenter
     *
     * @param \AppBundle\Entity\WorkCenter $workcenter
     *
     * @return Device
     */
    public function setWorkcenter(
      \AppBundle\Entity\WorkCenter $workcenter = null
    ) {
        $this->workcenter = $workcenter;

        return $this;
    }

    /**
     * Get workcenter
     *
     * @return \AppBundle\Entity\WorkCenter
     */
    public function getWorkcenter()
    {
        return $this->workcenter;
    }

    /**
     * @var \AppBundle\Entity\Operator
     */
    private $operator;

    /**
     * Set operator
     *
     * @param \AppBundle\Entity\Operator $operator
     *
     * @return Device
     */
    public function setOperator(\AppBundle\Entity\Operator $operator = null)
    {
        $this->operator = $operator;

        return $this;
    }

    /**
     * Get operator
     *
     * @return \AppBundle\Entity\Operator
     */
    public function getOperator()
    {
        return $this->operator;
    }

    /**
     * @var \AppBundle\Entity\Terminal
     */
    private $terminal;


    /**
     * Set terminal
     *
     * @param \AppBundle\Entity\Terminal $terminal
     *
     * @return Device
     */
    public function setTerminal(\AppBundle\Entity\Terminal $terminal = null)
    {
        $this->terminal = $terminal;

        return $this;
    }

    /**
     * Get terminal
     *
     * @return \AppBundle\Entity\Terminal
     */
    public function getTerminal()
    {
        return $this->terminal;
    }
}
