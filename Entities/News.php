<?php

namespace Kirasa\Module\ContentBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Kirasa\Column;

/**
 * News
 */
class News
{

    use Column\Id;
    use Column\Site;
    use Column\Title;
    use Column\Text;
    use Column\Image;
    use Column\Date;
    use Column\IsGeneral;
    use Column\IsDeleted;

    use Column\Language;
    use ORMBehaviors\Translatable\Translatable;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $sites;

    /**
     * @var Tag[]|ArrayCollection
     */
    private $tags;

    /**
     * @var bool
     */
    private $isLocalOrigin;

    /**
     * @return mixed
     */
    public function getIsLocalOrigin()
    {
        return $this->isLocalOrigin;
    }

    /**
     * @param mixed $isLocalOrigin
     */
    public function setIsLocalOrigin($isLocalOrigin)
    {
        $this->isLocalOrigin = $isLocalOrigin;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->sites = new ArrayCollection();
        $this->tags = new ArrayCollection();
    }

    /**
     * Add tag
     *
     * @param \Kirasa\Module\ContentBundle\Entity\Tag $tag
     */
    public function addTag(Tag $tag)
    {
        if (!$this->tags->contains($tag)) {
            $this->tags->add($tag);
        }
    }

    /**
     * Remove tag
     *
     * @param \Kirasa\Module\ContentBundle\Entity\Tag $tag
     */
    public function removeTag(Tag $tag)
    {
        $this->tags->removeElement($tag);
    }

    /**
     * Get Tags
     *
     * @return \Doctrine\Common\Collections\ArrayCollection|\Kirasa\Module\ContentBundle\Entity\Tag[]
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Add site
     *
     * @param \Kirasa\Core\PanelBundle\Entity\SiteConfig $site
     *
     * @return News
     */
    public function addSite(\Kirasa\Core\PanelBundle\Entity\SiteConfig $site)
    {
        $this->sites[] = $site;

        return $this;
    }

    /**
     * Remove site
     *
     * @param \Kirasa\Core\PanelBundle\Entity\SiteConfig $site
     */
    public function removeSite(\Kirasa\Core\PanelBundle\Entity\SiteConfig $site)
    {
        $this->sites->removeElement($site);
    }

    /**
     * Get sites
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSites()
    {
        return $this->sites;
    }

    /**
     * @var bool
     */
    protected $stickTop;

    /**
     * Set stickTop
     *
     * @param $stickTop
     *
     * @return $this
     */
    public function setStickTop($stickTop)
    {
        $this->stickTop = $stickTop;

        return $this;
    }

    /**
     * Get stickTop
     *
     * @return bool
     */
    public function getStickTop()
    {
        return $this->stickTop;
    }
}
