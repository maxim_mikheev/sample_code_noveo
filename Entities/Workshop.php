<?php

namespace AppBundle\Entity;

/**
 * Workshop
 */
class Workshop
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $workshop_id;

    /**
     * @return string
     */
    public function getWorkshopId()
    {
        return $this->workshop_id;
    }

    /**
     * @param string $workshop_id
     */
    public function setWorkshopId($workshop_id)
    {
        $this->workshop_id = $workshop_id;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Workshop
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Workshop
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $devices;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->devices = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add device
     *
     * @param \AppBundle\Entity\Device $device
     *
     * @return Workshop
     */
    public function addDevice(\AppBundle\Entity\Device $device)
    {
        $this->devices[] = $device;

        return $this;
    }

    /**
     * Remove device
     *
     * @param \AppBundle\Entity\Device $device
     */
    public function removeDevice(\AppBundle\Entity\Device $device)
    {
        $this->devices->removeElement($device);
    }

    /**
     * Get devices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDevices()
    {
        return $this->devices;
    }

    public function __toString()
    {
        return $this->title;
    }
    /**
     * @var \AppBundle\Entity\Department
     */
    private $department;


    /**
     * Set department
     *
     * @param \AppBundle\Entity\Department $department
     *
     * @return Workshop
     */
    public function setDepartment(\AppBundle\Entity\Department $department = null)
    {
        $this->department = $department;

        return $this;
    }

    /**
     * Get department
     *
     * @return \AppBundle\Entity\Department
     */
    public function getDepartment()
    {
        return $this->department;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $workcenters;


    /**
     * Add workcenter
     *
     * @param \AppBundle\Entity\WorkCenter $workcenter
     *
     * @return Workshop
     */
    public function addWorkcenter(\AppBundle\Entity\WorkCenter $workcenter)
    {
        $this->workcenters[] = $workcenter;

        return $this;
    }

    /**
     * Remove workcenter
     *
     * @param \AppBundle\Entity\WorkCenter $workcenter
     */
    public function removeWorkcenter(\AppBundle\Entity\WorkCenter $workcenter)
    {
        $this->workcenters->removeElement($workcenter);
    }

    /**
     * Get workcenters
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWorkcenters()
    {
        return $this->workcenters;
    }

}
