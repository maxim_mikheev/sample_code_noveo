<?php

namespace Kirasa\Module\ContentBundle\Form;

use Kirasa\Core\PanelBundle\Entity\Repository\SiteConfigRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewsType extends EntityBaseType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $sid = $options['current_sid'];

        $builder
            ->add('title', null, ['label' => 'Заголовок'])
            ->add('stickTop', null, ['label' => 'Закрепить на главной'])
            ->add('isLocalOrigin', null, ['label' => 'Это новость главного сайта'])
            ->add('date', DateType::class, ['label' => 'Дата', 'widget' => 'single_text', 'format' => 'dd-MM-yyyy', 'attr'=> ['autocomplete'=>'off']])
            ->add('file', FileType::class, ['label' => 'Изображение', 'required' => false])
            ->add('imgAlt', null, ['label' => 'Alt для изображения', 'required' => true])
            ->add('text', TextareaType::class, ['label' => 'Текст'])
              ->add('tags', TagsInputType::class, [
                'label' => 'Тэги',
                'required' => false,
              ])
            ->add('isGeneral', null, ['label' => 'Главная новость', 'required' => false]);
        if ($options['is_current_main_site']) {
            $builder->add('sites', EntityType::class, array(
                'class' => 'PanelBundle:SiteConfig',
                'choice_label' => 'host',
                'multiple' => true,
                'label' => 'Сайты',
                'expanded' => true,
                'query_builder' => function (SiteConfigRepository $repository) use ($sid) {
                    return $repository->getSitesNotMainOrCurrent($sid);
                }));
        }

        $builder->add('language', TextType::class, ['label' => 'Язык', 'disabled' => 'disabled']);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Kirasa\Module\ContentBundle\Entity\News',
            'current_sid' => null,
            'is_current_main_site' => null,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'kirasa_module_contentbundle_news';
    }
}
