<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class IndexController extends BaseController
{
    public function frontPageAction()
    {
        return $this->render('AppBundle:Index:front_page.html.twig', array());
    }

    public function panelAction()
    {
        return $this->render('AppBundle:Index:panel.html.twig', array());
    }

  /**
   * Input action for data flow from device aggregator
   * Receives json data and handle it
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return \Symfony\Component\HttpFoundation\Response
   */
    public function inputAction(Request $request)
    {
        $jsonData = $request->getContent();

        // test data
        //$jsonData = '{"device":"TC","password":"123","type":"km5_simulator","sensors_data":[{"name":"s_g1","time":0,"values":[]},{"name":"s_g2","time":0,"values":[]}],"commands_results":[{"id":"1","cmd":"subscribe","result":"ok","data":"s_g1"},{"id":"2","cmd":"subscribe","result":"ok","data":"s_g2"}],"hash":"85a4d4259a3e829356ec5a8da6ccce91"}';
        //$jsonData = '{"device":"TC","password":"123","type":"km5_simulator","sensors_data":[{"name":"s_g1","time":1494483086533,"values":[{"t":0,"v":83.462021}]},{"name":"s_g2","time":1494483086533,"values":[{"t":0,"v":70.799889}]}],"commands_results":[],"hash":"d4e39c4a08da65143769d3a82133290c"}';
        //$jsonData = '{"device":"SMTO","password":"123","type":"SMTO","sensors_data":[{"name":"routes","time":1497845496965,"values":[{"t":0,"v":8015}]},{"name":"state","time":1497845495093,"values":[{"t":0,"v":1}]}],"commands_results":[],"hash":"04ec2d215bbe209ba0530501356fb92e"}';
        $decodedData = json_decode($jsonData, true);

        $parser = $this->get('helper.parser.data');
        $securityData = $parser->getDeviceCodeAndPassword($decodedData);
        $device = $this->get('content.device.service')->getByCodeAndPassword($securityData['code'], $securityData['password']);

        $commands = [];

        if($device) {
            $parser->parse($decodedData);
            $commands = $this->get('content.command.service')->getCreatedByDevice($device);

            // Make ids string because emulator doesn't like integers
            array_walk($commands, function(&$elem) {
                $elem['id'] = (string)$elem['id'];
            });
        }

        $result = [];
        $result['error'] = 0;
        $result['commands'] = $commands;

        $response = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    public function testInputAction()
    {

        $json = file_get_contents('zdata.json');

        $data = json_decode($json, true);

        $this->get('helper.parser.data')->parse($data);

        $result = array();

        $device = $this->get('content.device.service')->getByCodeAndPassword($data['device'], $data['password']);

        if($device) {
            $commands = $this->get('content.command.service')->getCreatedByDevice($device);
            if($commands) {
                $result = $commands;
            }
        }

        $response = new Response(json_encode($result));

        return $response;
    }
}
