<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Operation;
use AppBundle\Entity\OperationalEvent;
use AppBundle\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


/**
 * Workshop controller.
 *
 * @Route("api/v1")
 */
class ApiController extends Controller
{

    const MAX_RESULTS = 1000;

    /**
     * Lists all
     *
     * @Route("/all", name="api_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $workshops = $this->getDoctrine()->getRepository('AppBundle:Department')->findAll();

        $data = [
            'departments' => $workshops
        ];

        return $this->json($data);
    }

    /**
     * Get device data
     *
     * @Route("/device/{id}", name="api_device")
     * @Method("GET")
     * @param $id
     * @return Response
     */
    public function deviceAction($id)
    {
        $device = $this->getDoctrine()->getRepository('AppBundle:Device')->find($id);

        $data = [
            'data' => $device
        ];

        return $this->json($data);
    }

    /**
     * Get device data
     *
     * @Route("/exploitation/events", name="api_get_exploitation")
     * @Method("GET")
     * @param Request $request
     * @return Response
     */
    public function getExploitationEventsAction(Request $request)
    {

        $valid_params_map = [
            'work_center_id',
            'workshop_id',
            'department_id',
            'operator_id',
            'device_id'
        ];

        $query = $request->query->all();

        $params = [];

        foreach ($query as $key => $value) {
            if (in_array($key, $valid_params_map)) {
                $params[$key] = $value;
            }
        }

        $events = $this->getDoctrine()->getRepository('AppBundle:ExploitationEvent')->findBy($params, ['event_timestamp' => 'DESC'], self::MAX_RESULTS);

        $this->log('get request');

        // example data from 1C
        /*$data = [
            'application/json' => [
                [
                    'event_timestamp' => ' yyyy-MM-dd\'T\'HH:mm:ss.SSSz',
                    'event_registration_timestamp' => 'string',
                    'event_type' => 'string', //'power_up'
                    'work_center_id' => 1,
                    'workshop_id' => 1,
                    'department_id' => 1,
                    'operator_id' => 1,
                ],
                [
                    'event_timestamp' => ' yyyy-MM-dd\'T\'HH:mm:ss.SSSz',
                    'event_registration_timestamp' => 'string',
                    'event_type' => 'string', //'power_up'
                    'work_center_id' => 1,
                    'workshop_id' => 1,
                    'department_id' => 1,
                    'operator_id' => 1,
                ],
            ]
        ];*/

        $data = [
            'application/json' => $events,
            'meta' => [
                'max_results' => self::MAX_RESULTS
            ]
        ];


        return $this->json($data);
    }

    /**
     * Put operationalEvents
     *
     * @Route("/operational/events", name="api_put_operational")
     * @Method({"POST"})
     * @param Request $request
     * @return Response
     */
    public function putOperationalEventsAction(Request $request)
    {

        $raw_data = trim($request->getContent());

        // Hack to avoid JSON parse error from 1C - remove all invisible symbols before "["
        $raw_data = mb_substr($raw_data, mb_strpos($raw_data, '[') );
        $data_array = json_decode($raw_data, true);
        $query = $request->query->all();

        // If data parsed correctly
        if (!empty($data_array) && !empty($query)) {
            $em = $this->getDoctrine()->getManager();

            foreach ($data_array as $row) {

                // Create new OperationalEvent
                $event = new OperationalEvent();
                $event->setEventTimestamp(new \DateTime($row['event_timestamp']));
                $event->setEventType($row['event_type']);
                $event->setOperatorId($query['operator_id']);
                $event->setWorkCenterId($query['work_center_id']);
                $event->setTerminalId($query['terminal_id']);
                $event->setOperationId($row['operation']['operation_id']);

                $operation = $this->getDoctrine()->getRepository('AppBundle:Operation')->findOneBy(['operation_id' => $row['operation']['operation_id']]);

                // If no such operation - create a new one
                if(!$operation) {
                    $operation = new Operation();
                    $operation->setOperationId($row['operation']['operation_id']);
                    $operation->setType($row['operation']['operation_type']);

                    $em->persist($operation);
                    $em->flush();
                }

                // Create products
                foreach ($row['operation']['production'] as $product_row) {
                    $product = $this->getDoctrine()->getRepository('AppBundle:Product')->findOneBy(['product_id' => $product_row]);

                    // If no product with this "product_id" - create new
                    if(!$product) {
                        $product = new Product();

                        // Id from 1C
                        $product->setProductId($product_row);

                        $em->persist($product);
                        $em->flush();
                    }
                }

                $em->persist($event);
                $em->flush();
            }
        }
        else {
            return new Response(json_encode(['error' => 'JSON Parse Error or No data passed']), Response::HTTP_UNPROCESSABLE_ENTITY, ['Content-type' => 'application/json']);
        }



        return new Response(null, Response::HTTP_NO_CONTENT, ['Content-type' => 'application/json']);
    }

    /**
     * Json JMS
     *
     * @param $data
     * @return Response
     */
    private function json($data) {

        $serializer = $this->get('jms_serializer');

        $response = new Response(
            $serializer->serialize($data, 'json'),
            Response::HTTP_OK,
            ['Content-type' => 'application/json']
        );

        return $response;
    }

  /**
   * Custom file log
   *
   * @param $data
   */
    private function log($data) {
        file_put_contents('log_api.txt', date('Y-m-d H:i:s') . ', new data:', FILE_APPEND);
        file_put_contents('log_api.txt', PHP_EOL, FILE_APPEND);
        file_put_contents('log_api.txt', print_r($data, true), FILE_APPEND);
        file_put_contents('log_api.txt', PHP_EOL, FILE_APPEND);
        file_put_contents('log_api.txt', PHP_EOL, FILE_APPEND);
    }


}
